/*******************************************************************************
 * Copyright (c) 2012 Rushan R. Gilmullin and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Rushan R. Gilmullin - initial API and implementation
 *******************************************************************************/
 package org.eclipse.osbp.vaaclipse.widgets;

import java.util.ArrayList;
import java.util.List;

import com.vaadin.shared.MouseEventDetails;
import com.vaadin.shared.ui.splitpanel.AbstractSplitPanelRpc;
import com.vaadin.ui.AbstractSplitPanel;
import com.vaadin.ui.AbstractSplitPanel.SplitterClickEvent;

/**
 * @author rushan
 *
 */
public class SashWidgetExtension {
	AbstractSplitPanel splitPanel;
	SashWidget sash;
	List<SplitPositionChangedListener> listeners = new ArrayList<SplitPositionChangedListener>();

	public SashWidgetExtension(AbstractSplitPanel splitPanel) {
		this.splitPanel = splitPanel;
		this.sash = (SashWidget) splitPanel;
		installRpc();
	}

	public void addListener(SplitPositionChangedListener listener) {
		this.listeners.add(listener);
	}

	public void fireEvent(float newPos) {
		for (SplitPositionChangedListener l : this.listeners) {
			l.processEvent(splitPanel, newPos);
		}
	}

	private void installRpc() {
		sash.registerRpc(new AbstractSplitPanelRpc() {

			@Override
			public void splitterClick(MouseEventDetails mouseDetails) {
				sash.fireEvent(new SplitterClickEvent(splitPanel, mouseDetails));
			}

			@Override
			public void setSplitterPosition(float position) {
				sash.getState().splitterState.position = position;
				System.out.println("split position changed!");
				fireEvent(splitPanel.getSplitPosition());
			}

		}, AbstractSplitPanelRpc.class);
	}
}
