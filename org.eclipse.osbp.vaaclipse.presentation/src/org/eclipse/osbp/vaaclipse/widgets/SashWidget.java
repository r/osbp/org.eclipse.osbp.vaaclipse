/*******************************************************************************
 * Copyright (c) 2012 Rushan R. Gilmullin and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Rushan R. Gilmullin - initial API and implementation
 *******************************************************************************/

 package org.eclipse.osbp.vaaclipse.widgets;

import java.util.EventObject;

import com.vaadin.shared.communication.ServerRpc;
import com.vaadin.shared.ui.splitpanel.AbstractSplitPanelState;

/**
 * @author rushan
 *
 */
public interface SashWidget {
	void addListener(SplitPositionChangedListener listener);

	void fireEvent(EventObject event);

	AbstractSplitPanelState getState();

	<T extends ServerRpc> void registerRpc(T implementation,
			Class<T> rpcInterfaceType);
}
