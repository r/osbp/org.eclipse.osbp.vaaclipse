/*******************************************************************************
 * Copyright (c) 2012 Rushan R. Gilmullin and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Rushan R. Gilmullin - initial API and implementation
 *******************************************************************************/
 package org.eclipse.osbp.vaaclipse;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * @author rushan
 *
 */
public class Activator implements BundleActivator {
	private static Activator instance;

	private BundleContext context;

	public void start(BundleContext context) throws Exception {
		instance = this;
		this.context = context;
	}

	public BundleContext getContext() {
		return context;
	}

	public static Activator getInstance() {
		return instance;
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		// TODO Auto-generated method stub

	}
}
