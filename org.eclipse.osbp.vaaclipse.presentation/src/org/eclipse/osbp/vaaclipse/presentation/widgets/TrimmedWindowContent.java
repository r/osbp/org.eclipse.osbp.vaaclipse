/*******************************************************************************
 * Copyright (c) 2012 Rushan R. Gilmullin and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Rushan R. Gilmullin - initial API and implementation
 *******************************************************************************/

package org.eclipse.osbp.vaaclipse.presentation.widgets;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;

/**
 * @author rushan
 * 
 */
public class TrimmedWindowContent extends VerticalLayout {
	private VerticalLayout windowBody;
	private VerticalLayout windowCenterArea;
	private HorizontalLayout helperLayout;

	private HorizontalLayout topContainerLayout = new HorizontalLayout();
	private HorizontalLayout leftContainerChild = new HorizontalLayout();
	private Panel middleContainerChild = new Panel();
	private HorizontalLayout rightContainerChild = new HorizontalLayout();

	private VerticalLayout leftBarContainer = new VerticalLayout();
	private VerticalLayout rightBarContainer = new VerticalLayout();
	private VerticalLayout topBarContainer = new VerticalLayout();
	private HorizontalLayout bottomBarContainer = new HorizontalLayout();

	private boolean boundsValide = false;

	public enum TopBarPosition {
		TOPBAR_LEFT, TOPBAR_RIGHT
	};

	public TrimmedWindowContent() {
		leftBarContainer.setWidth(-1, Unit.PIXELS);
		leftBarContainer.setHeight("100%");

		rightBarContainer.setWidth(-1, Unit.PIXELS);
		rightBarContainer.setHeight("100%");

		topBarContainer.setHeight(-1, Unit.PIXELS);
		topBarContainer.setWidth("100%");
		topBarContainer.setMargin(false);

		// bottomBarContainer.setSizeFull();
		bottomBarContainer.setHeight(-1, Unit.PIXELS);
		bottomBarContainer.setWidth("100%");
		bottomBarContainer.setMargin(false);

		this.setSizeFull();

		windowBody = new VerticalLayout();
		windowBody.setSizeFull();
		this.addComponent(windowBody);
		this.setExpandRatio(windowBody, 100);

		windowCenterArea = new VerticalLayout();
		windowCenterArea.setSizeFull();

		helperLayout = new HorizontalLayout();
		helperLayout.setSizeFull();

		topContainerLayout.setWidth("100%");
		windowBody.addComponent(topContainerLayout);
		
		topContainerLayout.addComponents(leftContainerChild, middleContainerChild, rightContainerChild);
		
		leftContainerChild.setWidth("250px");
		topContainerLayout.setComponentAlignment(leftContainerChild, Alignment.TOP_LEFT);
		
		middleContainerChild.setSizeFull();
		middleContainerChild.addStyleName("os-perspective-stack-slot");
		topContainerLayout.setExpandRatio(middleContainerChild, 1.0f);
		
		topContainerLayout.setComponentAlignment(rightContainerChild, Alignment.TOP_RIGHT);

		// ------------------------
		helperLayout.addComponent(leftBarContainer);
		helperLayout.addComponent(windowCenterArea);
		helperLayout.addComponent(rightBarContainer);
		helperLayout.setExpandRatio(windowCenterArea, 100);

		windowBody.addComponent(topBarContainer);
		windowBody.addComponent(helperLayout);
		windowBody.setExpandRatio(helperLayout, 100);
		windowBody.addComponent(bottomBarContainer);
		// -------------------------------------------------------------------
	}

	public VerticalLayout getClientArea() {
		return windowCenterArea;
	}

	public void setMenuBar(MenuBar menuBar) {
		for (int i = 0; i < this.getComponentCount(); i++) {
			Component c = this.getComponent(i);
			if (c instanceof MenuBar)
				this.removeComponent(c);
		}

		menuBar.setWidth("100%");
		this.addComponent(menuBar, 0);
	}

	public HorizontalLayout getPerspectiveStackPanel() {
		return leftContainerChild;
	}

	public void setPerspectiveStackPanel(HorizontalLayout perspectiveStackPanel) {
		if (perspectiveStackPanel == null) {
			middleContainerChild.setContent(null);
		} else {
			perspectiveStackPanel.setSizeUndefined();
			middleContainerChild.setContent(perspectiveStackPanel);
		}
	}

	public void setLeftBar(Component bar) {
		if (bar == null) {
			leftBarContainer.removeAllComponents();
			return;
		}

		leftBarContainer.removeAllComponents();
		leftBarContainer.addComponent(bar);
	}

	public void setRightBar(Component bar) {
		if (bar == null) {
			rightBarContainer.removeAllComponents();
			return;
		}

		rightBarContainer.removeAllComponents();
		rightBarContainer.addComponent(bar);
	}

	public void setBottomBar(Component bar) {
		if (bar == null) {
			bottomBarContainer.removeAllComponents();
			return;
		}

		bottomBarContainer.removeAllComponents();
		bottomBarContainer.addComponent(bar);
	}

	public void setTopBar(Component bar, TopBarPosition pos) {
		Alignment align = Alignment.MIDDLE_LEFT;
		if (pos == TopBarPosition.TOPBAR_RIGHT) {
			align = Alignment.MIDDLE_RIGHT;
			if (bar == null) {
				rightContainerChild.removeAllComponents();
			}else {
				rightContainerChild.addComponent(bar);
			}
		} else {
			if (bar == null) {
				leftContainerChild.removeAllComponents();
			}else {
				leftContainerChild.addComponent(bar);
			}
		}
	}

	public Component getTopbar(TopBarPosition pos) {
		if (pos == TopBarPosition.TOPBAR_RIGHT) {
			return rightContainerChild;
		} else {
			return leftContainerChild;
		}
	}

	// -----------------------------------
	// -----------------------------------
	public boolean isBoundsValid() {
		return this.boundsValide;
	}

	public void invalidateBounds() {
		this.boundsValide = false;
	}
}
