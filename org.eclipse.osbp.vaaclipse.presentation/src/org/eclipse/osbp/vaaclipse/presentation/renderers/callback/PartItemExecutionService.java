/*******************************************************************************
 * Copyright (c) 2012 Rushan R. Gilmullin and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Rushan R. Gilmullin - initial API and implementation
 *******************************************************************************/
 package org.eclipse.osbp.vaaclipse.presentation.renderers.callback;

import org.eclipse.e4.ui.model.application.ui.menu.MItem;
import org.eclipse.osbp.vaaclipse.presentation.renderers.ItemRenderer;
import org.eclipse.osbp.vaaclipse.publicapi.commands.IPartItemExecutionService;

public class PartItemExecutionService implements IPartItemExecutionService {

	public PartItemExecutionService() {
	}

	@Override
	public boolean canExecuteItem(MItem mItem) {
		Object renderer = mItem.getRenderer();
		if (renderer instanceof ItemRenderer) {
			ItemRenderer itemRenderer = (ItemRenderer) renderer;
			return itemRenderer.canExecute(mItem);
		}
		return true;
	}

	@Override
	public void executeItem(MItem mItem) {
		Object renderer = mItem.getRenderer();
		if (renderer instanceof ItemRenderer) {
			ItemRenderer itemRenderer = (ItemRenderer) renderer;
			itemRenderer.executeItem(mItem);
		}
	}

}
