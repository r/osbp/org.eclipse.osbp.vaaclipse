/*******************************************************************************
 * Copyright (c) 2012 Rushan R. Gilmullin and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Rushan R. Gilmullin - initial API and implementation
 *******************************************************************************/

package org.eclipse.osbp.vaaclipse.presentation.renderers;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;

import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.services.contributions.IContributionFactory;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.model.application.ui.MElementContainer;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.model.application.ui.basic.MInputPart;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.basic.MPartStack;
import org.eclipse.e4.ui.model.application.ui.menu.MToolBar;
import org.eclipse.e4.ui.workbench.IPresentationEngine;
import org.eclipse.e4.ui.workbench.UIEvents;
import org.eclipse.e4.ui.workbench.UIEvents.EventTags;
import org.eclipse.osbp.vaaclipse.presentation.renderers.callback.PartItemExecutionService;
import org.eclipse.osbp.vaaclipse.publicapi.commands.IPartItemExecutionService;
import org.eclipse.osbp.vaaclipse.publicapi.editor.SavePromptSetup;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventHandler;

import com.vaadin.event.LayoutEvents.LayoutClickEvent;
import com.vaadin.ui.AbstractLayout;
import com.vaadin.ui.Component;
import com.vaadin.ui.Component.Focusable;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("restriction")
public class PartRenderer extends VaadinRenderer {

	private Map<MPart, SavePromptSetup> savePrompts = new HashMap<MPart, SavePromptSetup>();

	@Inject
	IPresentationEngine renderingEngine;

	@Inject
	IEventBroker eventBroker;

	private EventHandler contributionURIChanged = new EventHandler() {
		@Override
		public void handleEvent(Event event) {
			Object changedObj = event.getProperty(EventTags.ELEMENT);
			if (!(changedObj instanceof MPart)) {
				return;
			}

			final MPart part = (MPart) changedObj;
			IEclipseContext localContext = part.getContext();
			if(localContext != null) {
				IContributionFactory contributionFactory = (IContributionFactory) localContext
						.get(IContributionFactory.class.getName());
				Object newPart = contributionFactory.create(part.getContributionURI(), localContext);
	
				if (part.getObject() != null) {
					ContextInjectionFactory.uninject(part.getObject(), localContext);
				}
				part.setObject(newPart);
			}
		}
	};

	public SavePromptSetup getSavePromptSetup(MPart part) {
		return savePrompts.get(part);
	}

	@SuppressWarnings({ "deprecation", "rawtypes" })
	@Override
	public void createWidget(MUIElement element, MElementContainer<MUIElement> parent) {
		Panel shortcutReceiver = new Panel();
		shortcutReceiver.setSizeFull();

		VerticalLayout pane = new VerticalLayout();
		shortcutReceiver.setContent(pane);
		pane.setSizeFull();
		final MPart part = (MPart) element;

		// toolbar
		MToolBar toolbar = part.getToolbar();
		if (toolbar != null && toolbar.isToBeRendered()) {
			// create toolbar area
			CssLayout toolbarArea = new CssLayout();
			toolbarArea.setStyleName("mparttoolbararea");
			toolbarArea.setSizeUndefined();
			toolbarArea.setWidth("100%");
			pane.addComponent(toolbarArea);

			// create toolbar
			Component toolbarWidget = (Component) renderingEngine.createGui(toolbar);
			((AbstractLayout) toolbarWidget).setSizeUndefined();
			toolbarWidget.setStyleName("mparttoolbar");
			toolbarArea.addComponent(toolbarWidget);
		}

		VerticalLayout contributionArea = new VerticalLayout();
		contributionArea.setSizeFull();
		pane.addComponent(contributionArea);
		pane.setExpandRatio(contributionArea, 100);

		pane.setStyleName("part");
		part.setWidget(shortcutReceiver);

		final IEclipseContext localContext = part.getContext();
		localContext.set(Component.class, contributionArea);
		localContext.set(ComponentContainer.class, contributionArea);
		localContext.set(VerticalLayout.class, contributionArea);
		localContext.set(MPart.class, part);
		localContext.set(IPartItemExecutionService.class, new PartItemExecutionService());

		pane.addLayoutClickListener(e -> {
			if(e.getClickedComponent() instanceof Focusable) {
				localContext.set(LayoutClickEvent.class, e);
			} else {
				localContext.set(LayoutClickEvent.class, null);
			}
			localContext.activate();
		});

		SavePromptSetup savePromptProvider = new SavePromptSetup();
		savePrompts.put(part, savePromptProvider);
		localContext.set(SavePromptSetup.class, savePromptProvider);
		if (part instanceof MInputPart)
			localContext.set(MInputPart.class, (MInputPart) part);

		boolean render = false;
		if (part.getParent() instanceof MElementContainer) {
			MElementContainer mParent = part.getParent();
			// workaround since MElementContainer<...> is not castable to
			// MPartStack
			if (mParent instanceof MPartStack) {
				MPartStack mStack = (MPartStack) mParent;
				if (mStack.getSelectedElement() == part
						|| !mStack.getChildren().isEmpty() && mStack.getChildren().get(0) == part
						|| mStack.getChildren().size() == 1) {
					// render the part eager
					render = true;
				}
			} else {
				// render the part eager
				render = true;
			}
		} else {
			// render the part eager
			render = true;
		}

		if (render) {
			renderPartContent(part, localContext);
		}
	}

	public static void renderPartContent(final MPart part, final IEclipseContext localContext) {
		IContributionFactory contributionFactory = (IContributionFactory) localContext
				.get(IContributionFactory.class.getName());
		Object newPart = contributionFactory.create(part.getContributionURI(), localContext);

		part.setObject(newPart);
	}

	@Override
	public void disposeWidget(MUIElement element) {
		this.savePrompts.remove(element);
	}

	@PostConstruct
	public void postConstruct() {
		eventBroker.subscribe(UIEvents.Contribution.TOPIC_CONTRIBUTIONURI, contributionURIChanged);
	}

	@PreDestroy
	public void dispose() {
		eventBroker.unsubscribe(contributionURIChanged);
	}

}
