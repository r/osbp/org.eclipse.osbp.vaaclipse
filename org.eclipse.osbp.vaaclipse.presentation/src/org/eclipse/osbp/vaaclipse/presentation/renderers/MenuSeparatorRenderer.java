/*******************************************************************************
 * Copyright (c) 2012 Rushan R. Gilmullin and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Rushan R. Gilmullin - initial API and implementation
 *******************************************************************************/
 package org.eclipse.osbp.vaaclipse.presentation.renderers;

import org.eclipse.e4.ui.model.application.ui.MElementContainer;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.model.application.ui.menu.MMenuSeparator;

import com.vaadin.ui.MenuBar.MenuItem;

/**
 * @author rushan
 *
 */
public class MenuSeparatorRenderer extends VaadinRenderer {
	@Override
	public void createWidget(MUIElement element,
			MElementContainer<MUIElement> parent) {
		if (!(element instanceof MMenuSeparator))
			return;

		MUIElement nextRenderableAndVisible = findNextRendarableAndVisible(
				element, parent);

		MenuItem separator = null;
		if (nextRenderableAndVisible == null)
			separator = ((MenuItem) parent.getWidget()).addSeparator();
		else
			separator = ((MenuItem) parent.getWidget())
					.addSeparatorBefore((MenuItem) nextRenderableAndVisible
							.getWidget());
		element.setWidget(separator);
	}

	@Override
	public void setVisible(MUIElement changedElement, boolean visible) {
		((MenuItem) changedElement.getWidget()).setVisible(visible);
	}
}
