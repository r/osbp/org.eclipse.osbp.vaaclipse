/*******************************************************************************
 * Copyright (c) 2012 Rushan R. Gilmullin and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Rushan R. Gilmullin - initial API and implementation
 *******************************************************************************/

package org.eclipse.osbp.vaaclipse.presentation.renderers;

import java.util.Map.Entry;

import javax.annotation.PreDestroy;
import javax.inject.Inject;

import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.model.application.ui.MElementContainer;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.model.application.ui.advanced.MPerspective;
import org.eclipse.e4.ui.model.application.ui.basic.MPartSashContainerElement;
import org.eclipse.e4.ui.model.application.ui.basic.MStackElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.osbp.runtime.designer.api.IDesignerService;
import org.eclipse.osbp.runtime.designer.api.IWidgetDesignConfigurator;

import com.vaadin.ui.Component;
import com.vaadin.ui.VerticalLayout;

/**
 * @author rushan
 *
 */
public class PerspectiveRenderer extends VaadinRenderer implements
		IDesignerService.IDesignListener {

	@Inject
	@Optional
	private IDesignerService designerService;

	@Override
	public void createWidget(MUIElement element,
			MElementContainer<MUIElement> parent) {
		if (!(element instanceof MPerspective))
			return;

		VerticalLayout perspectivePanel = new VerticalLayout();
		perspectivePanel.setSizeFull();
		element.setWidget(perspectivePanel);

		MPerspective p = (MPerspective) element;

		if (p.getIconURI() == null && p.getLabel() == null) {
			p.setIconURI("platform:/plugin/org.eclipse.osbp.vaaclipse.resources/VAADIN/themes/vaaclipse_default_theme/img/blank_perspective.png");
		}

		if (designerService != null) {
			designerService.addListener(this);

			if (designerService.isDesignMode()) {
				updateDesigner(true);
			}
		}
	}
	
	@PreDestroy
	public void preDestroy() {

		if (designerService != null) {
			designerService.removeListener(this);
		}
	}

	@Override
	public void notify(IDesignerService.DesignEvent event) {
		updateDesigner(event.getType() == IDesignerService.EventType.ENABLED);
	}

	private void updateDesigner(boolean enabled) {
		IWidgetDesignConfigurator designConfigurator = context
				.get(IWidgetDesignConfigurator.class);
//		if (designConfigurator != null) {
//			for (Entry<Component, MStackElement> entry : vaatab2Element
//					.entrySet()) {
//				designConfigurator.configure(entry.getKey(),
//						(EObject) entry.getValue(), enabled);
//			}
//		}
	}

	@Override
	public void processContents(MElementContainer<MUIElement> element) {
		VerticalLayout perspectivePanel = (VerticalLayout) element.getWidget();
		for (MUIElement e : element.getChildren()) {
			if (e.isToBeRendered() && e.getWidget() != null) {
				perspectivePanel.addComponent((Component) e.getWidget());
			}
		}
	}

	@Override
	public void addChildGui(MUIElement child,
			MElementContainer<MUIElement> element) {
		if (!(child instanceof MPartSashContainerElement))
			return;

		VerticalLayout sw = (VerticalLayout) element.getWidget();
		int index = indexOf(child, element);
		sw.addComponent((Component) child.getWidget(), index);
		
		if(designerService != null && designerService.isDesignMode()) {
			updateDesigner(designerService.isDesignMode());
		}
	}
}
