/*******************************************************************************
 * Copyright (c) 2011 Kai Toedter and others.
 * 
 * All rights reserved. This program and the accompanying materials 
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/.
 *.
 * SPDX-License-Identifier: EPL-2.0.
 * 
 * Contributors:
 *     Kai Toedter - initial API and implementation
 ******************************************************************************/

package org.eclipse.osbp.vaaclipse.presentation.engine;

import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.osbp.vaaclipse.presentation.renderers.GenericRenderer;
import org.eclipse.osbp.vaaclipse.presentation.renderers.VaadinRenderer;

@SuppressWarnings("restriction")
public class GenericRendererFactory implements RendererFactory {

	VaadinRenderer genericRenderer = new VaadinRenderer();

	@Override
	public GenericRenderer getRenderer(MUIElement uiElement) {
		return genericRenderer;
	}

}
