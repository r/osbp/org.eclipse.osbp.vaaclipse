/*******************************************************************************
 * Copyright (c) 2012 Rushan R. Gilmullin and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Rushan R. Gilmullin - initial API and implementation
 *******************************************************************************/

package org.eclipse.osbp.e4extension;

import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.osbp.e4extension.impl.PartServiceExtImpl;
import org.eclipse.osbp.e4extension.service.EPartServiceExt;

/**
 * @author rushan
 *
 */
public class StartServiceProcessor {
	@Execute
	void addMinMaxAddon(MApplication application, IEclipseContext context) {

		PartServiceExtImpl partServiceExt = ContextInjectionFactory.make(
				PartServiceExtImpl.class, context);
		context.set(EPartServiceExt.class, partServiceExt);
	}
}
