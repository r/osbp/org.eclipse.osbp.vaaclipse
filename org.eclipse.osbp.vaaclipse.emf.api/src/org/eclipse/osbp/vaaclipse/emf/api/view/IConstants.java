/*******************************************************************************
 * Copyright (c) 2012 Rushan R. Gilmullin and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Rushan R. Gilmullin - initial API and implementation
 *******************************************************************************/
 package org.eclipse.osbp.vaaclipse.emf.api.view;

import org.eclipse.osbp.vaadin.emf.api.IModelingConstants;

public interface IConstants extends IModelingConstants {

	/**
	 * This topic is used send an EObject as an input to the tree viewer.
	 */
	public static String TOPIC__TREE_INPUT_IN = "osbee/vaaclipse/emf/tree/input";

	/**
	 * This topic is used send an EObject that was selected by the tree viewer.
	 */
	public static String TOPIC__TREE_SELECTION_OUT = "osbee/vaaclipse/emf/tree/selection";

	/**
	 * Tag used to mark all designer parts in the e4 application model.
	 */
	public static String TAG__DESIGNER_PARTS = "designer:part";

	/**
	 * Tag used to mark designer parts which should only become rendered if the
	 * design mode is active.
	 */
	public static String TAG__DESIGNER_AUTO_VISIBLE = "designer:autoVisible";
}
