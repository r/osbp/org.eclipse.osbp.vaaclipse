/*******************************************************************************
 * Copyright (c) 2012 Rushan R. Gilmullin and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Rushan R. Gilmullin - initial API and implementation
 *******************************************************************************/
 package org.eclipse.osbp.vaaclipse.emf.api.perspective;

import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.ui.model.application.ui.advanced.MPerspective;

/**
 * Provides layouts for a given perspective.
 */
public interface IPerspectiveLayoutProvider {

	/**
	 * Returns the description for the provider.
	 * 
	 * @return
	 */
	String getDescription(IEclipseContext context);

	/**
	 * Is called to layout the given perspective. At this point in time, the
	 * perspective is not added to the stack. You do not have to do so. It will
	 * be done automatically if the perspective was added.
	 * 
	 * @param context
	 * @param stack
	 * @param perspective
	 */
	void layout(IEclipseContext context, MPerspective perspective);

	/**
	 * This method is called after the perspective has been added to the stack.
	 * 
	 * @param context
	 * @param perspective
	 */
	void postAdded(IEclipseContext context, MPerspective perspective);
}
