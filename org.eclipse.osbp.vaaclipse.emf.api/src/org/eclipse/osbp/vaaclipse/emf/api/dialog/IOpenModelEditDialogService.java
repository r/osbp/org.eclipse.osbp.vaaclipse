/*******************************************************************************
 * Copyright (c) 2012 Rushan R. Gilmullin and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Rushan R. Gilmullin - initial API and implementation
 *******************************************************************************/
 package org.eclipse.osbp.vaaclipse.emf.api.dialog;

import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.emf.ecore.EObject;

/**
 * 
 * @NoImplementByClients use {@link Participant}
 */
public interface IOpenModelEditDialogService {

	/**
	 * The advanced properties dialog. Also advanced properties are being shown.
	 */
	public static String TYPE_ADVANCED = "advanced";

	/**
	 * The simple properties dialog. Only few properties are being shown.
	 */
	public static String TYPE_SIMPLE = "simple";

	/**
	 * Opens an dialog to edit the given modelObjects properties.
	 * 
	 * @param modelObject
	 * @param type
	 *            the type the dialog to be opened
	 * @param context
	 */
	void openEditDialog(EObject modelObject, String type,
			IEclipseContext context);

	interface Participant {

		/**
		 * Opens an dialog to edit the given modelObjects properties.
		 * 
		 * @param modelObject
		 * @param type
		 *            the type the dialog to be opened
		 * @param context
		 */
		void openEditDialog(EObject modelObject, String type,
				IEclipseContext context);

		/**
		 * Returns true, if this instance handles the given modelObject.
		 * 
		 * @param modelObject
		 * @param type
		 *            the type the dialog to be opened
		 * @param context
		 */
		boolean isFor(EObject modelObject, String type, IEclipseContext context);

	}

}
