/*******************************************************************************
 * Copyright (c) 2012 Rushan R. Gilmullin and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Rushan R. Gilmullin - initial API and implementation
 *******************************************************************************/ package org.eclipse.osbp.vaaclipse.publicapi.change;

import org.eclipse.emf.common.command.AbstractCommand;

/**
 * Uses the {@link SimpleCommand} to record notifications.
 */
public abstract class SimpleCommand extends AbstractCommand {

	public SimpleCommand(String name) {
		super(name);
	}

	@Override
	public void execute() {
		doExecute();
	}

	@Override
	public void redo() {
		doRedo();
	}

	@Override
	protected boolean prepare() {
		return true;
	}

	@Override
	public void undo() {
		doUndo();
	}

	@Override
	public boolean canUndo() {
		return true;
	}

	protected abstract void doExecute();

	/**
	 * Override if required.
	 */
	protected void doRedo() {
		doExecute();
	}

	protected abstract void doUndo();

}
