/*******************************************************************************
 * Copyright (c) 2012 Rushan R. Gilmullin and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Rushan R. Gilmullin - initial API and implementation
 *******************************************************************************/
 package org.eclipse.osbp.vaaclipse.publicapi.change;

import org.eclipse.emf.common.command.AbstractCommand;
import org.eclipse.emf.ecore.resource.Resource;

/**
 * Uses the {@link ChangeCommand} to record notifications.
 */
public abstract class ChangeCommand extends AbstractCommand {

	private Resource resource;
	private ChangedRecorder recorder;

	public ChangeCommand(String name, Resource resource) {
		super(name);
		this.resource = resource;
	}

	@Override
	public void execute() {
		recorder.start();

		doExecute();

		recorder.stop();
	}
	
	/**
	 * Execute your operations.
	 */
	protected abstract void doExecute();

	@Override
	public void redo() {
		recorder.redo();
	}

	@Override
	protected boolean prepare() {
		recorder = new ChangedRecorder(resource);
		return true;
	}

	@Override
	public void undo() {
		recorder.undo();
	}

	@Override
	public boolean canUndo() {
		return super.canUndo();
	}

}
