/*******************************************************************************
 * Copyright (c) 2012 Rushan R. Gilmullin and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Rushan R. Gilmullin - initial API and implementation
 *******************************************************************************/
 package org.eclipse.osbp.vaaclipse.publicapi.theme;

import java.util.List;

/**
 * @author rushan
 *
 */
public interface ThemeEntry {
	/**
	 * Theme id
	 */
	String getId();

	/**
	 * Web id - this id is in uri (replace all dot's, etc)
	 */
	String getWebId();

	String getCssUri();

	List<String> getResourceLocationURIs();
}
