/*******************************************************************************
 * Copyright (c) 2012 Rushan R. Gilmullin and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Rushan R. Gilmullin - initial API and implementation
 *******************************************************************************/
 package org.eclipse.osbp.vaaclipse.publicapi.preferences;

import org.osgi.service.prefs.Preferences;

/**
 * A service that provides preferences.
 */
public interface IPreferenceProvider {
	
	/**
	 * Returns the system preferences.
	 * 
	 * @return
	 */
	Preferences getSystemPreferences();

	/**
	 * Returns the preferences for the current user.
	 * 
	 * @return
	 */
	Preferences getUserPreferences();

	/**
	 * Returns the preferences for the given user.
	 * 
	 * @return
	 */
	Preferences getUserPreferences(String userId);

}
