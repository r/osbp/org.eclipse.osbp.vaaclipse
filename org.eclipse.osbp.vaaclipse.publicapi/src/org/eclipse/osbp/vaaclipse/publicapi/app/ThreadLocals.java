/*******************************************************************************
 * Copyright (c) 2012 Rushan R. Gilmullin and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Rushan R. Gilmullin - initial API and implementation
 *******************************************************************************/
package org.eclipse.osbp.vaaclipse.publicapi.app;

import org.eclipse.e4.core.contexts.IEclipseContext;

/**
 * @author rushan
 *
 */
public class ThreadLocals {
	private static final ThreadLocal<IEclipseContext> eclipseContext = new ThreadLocal<IEclipseContext>();

	public static IEclipseContext getRootContext() {
		return eclipseContext.get();
	}

	public static void setRootContext(IEclipseContext context) {
		eclipseContext.set(context);
	}

}
