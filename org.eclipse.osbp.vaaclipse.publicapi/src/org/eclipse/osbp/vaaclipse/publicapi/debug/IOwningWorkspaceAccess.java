/*******************************************************************************
 * Copyright (c) 2012 Rushan R. Gilmullin and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Rushan R. Gilmullin - initial API and implementation
 *******************************************************************************/
 package org.eclipse.osbp.vaaclipse.publicapi.debug;

public interface IOwningWorkspaceAccess {

	/**
	 * In debug mode, this value may be used to access the URI of the launching
	 * workspace location
	 */
	public static final String DEBUG_OWNER_WORKSPACE_LOCATION = "debugOwnerWorkspaceLocation";
	
	
}
