/*******************************************************************************
 * Copyright (c) 2012 Rushan R. Gilmullin and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Rushan R. Gilmullin - initial API and implementation
 *******************************************************************************/
package org.eclipse.osbp.vaaclipse.publicapi.resources;

import com.vaadin.server.ThemeResource;

/**
 * @author rushan
 */
public class ResourceHelper {
	public static ThemeResource createResource(String path) {
		if (path == null || path.trim().equals("")) {
			return null;
		}
		if (path.startsWith("platform:/plugin/"))
			return BundleResource.valueOf(path);
		else
			return new ThemeResource(path);
	}
}
