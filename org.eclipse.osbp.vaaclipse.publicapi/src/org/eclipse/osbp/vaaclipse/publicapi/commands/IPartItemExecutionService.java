/*******************************************************************************
 * Copyright (c) 2012 Rushan R. Gilmullin and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Rushan R. Gilmullin - initial API and implementation
 *******************************************************************************/
 package org.eclipse.osbp.vaaclipse.publicapi.commands;

import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.menu.MItem;

/**
 * {@link MPart parts} provide this service by their eclipse context. It can be
 * used to execute {@link MItem} directly.
 */
public interface IPartItemExecutionService {

	/**
	 * Executes the given item in the context of the container part.
	 * 
	 * @param mItem
	 * @return true if the item can be executed. False otherwise.
	 */
	boolean canExecuteItem(MItem mItem);
	
	/**
	 * Executes the given item in the context of the container part.
	 * 
	 * @param mItem
	 */
	void executeItem(MItem mItem);

}
