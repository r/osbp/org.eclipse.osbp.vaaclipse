/*******************************************************************************
 * Copyright (c) 2012 Rushan R. Gilmullin and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Rushan R. Gilmullin - initial API and implementation
 *******************************************************************************/
package org.eclipse.osbp.vaaclipse.publicapi.events;

public class VaaclipseUiEvents {

	public static interface Item {
		public static final String TOPIC_EXECUTED = "org/eclipse/e4/ui/model/menu/Item/executed"; //$NON-NLS-1$
	}

}
