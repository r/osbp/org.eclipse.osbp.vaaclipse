/*******************************************************************************
 * Copyright (c) 2012 Rushan R. Gilmullin and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Rushan R. Gilmullin - initial API and implementation
 *******************************************************************************/
 package org.eclipse.osbp.vaaclipse.publicapi.authentication;

/**
 * @author rushan
 *
 */
public class AuthenticationConstants {

	/**
	 * For e4 context. User id as string.
	 */
	public static final String USER_ID = "userId";

	/**
	 * For e4 context. Needs to implement IUser.
	 */
	public static final String USER = "user";

	public static class Events {

		public static class Authentication {
			public static final String name = "vaaclipseAuthentication";
			public static final String registration = "vaaclipseRegistration";
			public static final String userClass = "userClass";
			/**
			 * Is sent before the logout of the current session.
			 */
			public static final String PRE_LOGOUT = "session/prelogout";
			/**
			 * Logs out the current session.
			 */
			public static final String LOGOUT = "session/logout";
		}
	}
}
