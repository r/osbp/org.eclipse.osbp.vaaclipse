/*******************************************************************************
 * Copyright (c) 2012 Rushan R. Gilmullin and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Rushan R. Gilmullin - initial API and implementation
 *******************************************************************************/
 package org.eclipse.osbp.commons.geom;

/**
 * Side values from eclipse model's SideValue for using in client and server
 * 
 * @author rushan
 *
 */
public class Side {
	public static final Integer TOP = 0;

	public static final Integer BOTTOM = 1;

	public static final Integer LEFT = 2;

	public static final Integer RIGHT = 3;

	public static final Integer CENTER = 100;
}
