/*******************************************************************************
 * Copyright (c) 2012 Rushan R. Gilmullin and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Rushan R. Gilmullin - initial API and implementation
 *******************************************************************************/

package org.eclipse.osbp.vaaclipse.behaviour;

import java.util.List;

import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.MAddon;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.impl.ApplicationFactoryImpl;

@SuppressWarnings("restriction")
public class MinMaxProcessor {
	private static final String BUNDLE_CLASS = "bundleclass://org.eclipse.osbp.vaaclipse.behaviour";

	public static final String MIN_MAX_CONTRIBUTION_URI = BUNDLE_CLASS
			+ "/org.eclipse.osbp.vaaclipse.behaviour.MinMaxAddon";

	@Execute
	void addMinMaxAddon(MApplication application, IEclipseContext context) {
		List<MAddon> addons = application.getAddons();

		// Prevent multiple copies
		for (MAddon addon : addons) {
			if (addon.getContributionURI().equals(MIN_MAX_CONTRIBUTION_URI)) {
				return;
			}
		}

		MAddon minMaxAddon = ApplicationFactoryImpl.eINSTANCE.createAddon();
		minMaxAddon.setElementId("MinMaxAddon");
		minMaxAddon.setContributionURI(MIN_MAX_CONTRIBUTION_URI);
		application.getAddons().add(minMaxAddon);
	}
}
