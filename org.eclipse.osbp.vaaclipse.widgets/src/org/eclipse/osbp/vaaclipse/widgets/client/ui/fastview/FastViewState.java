/*******************************************************************************
 * Copyright (c) 2012 Rushan R. Gilmullin and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Rushan R. Gilmullin - initial API and implementation
 *******************************************************************************/
 package org.eclipse.osbp.vaaclipse.widgets.client.ui.fastview;

import com.vaadin.shared.Connector;
import com.vaadin.shared.ui.window.WindowState;

/**
 * @author rushan
 *
 */
public class FastViewState extends WindowState {
	public Integer side;
	public Connector trimmedWindowClientArea;
}
