/*******************************************************************************
 * Copyright (c) 2012 Rushan R. Gilmullin and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Rushan R. Gilmullin - initial API and implementation
 *******************************************************************************/

package org.eclipse.osbp.vaaclipse.widgets;

import org.eclipse.osbp.vaaclipse.widgets.client.ui.fastview.FastViewState;

import com.vaadin.ui.Component;
import com.vaadin.ui.Window;

/**
 * @author rushan
 *
 */
public class FastView extends Window {
	@Override
	protected FastViewState getState() {
		return (FastViewState) super.getState();
	}

	public void setSide(Integer side) {
		this.getState().side = side;
	}

	public void setTrimmedWindowClientArea(Component component) {
		this.getState().trimmedWindowClientArea = component;
	}
}
