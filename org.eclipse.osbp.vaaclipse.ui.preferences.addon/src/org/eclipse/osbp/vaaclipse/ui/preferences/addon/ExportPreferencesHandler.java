/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.vaaclipse.ui.preferences.addon;

import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.osbp.vaaclipse.ui.preferences.addon.internal.impexp.ExportPreferences;

/**
 * @author rushan
 *
 */
public class ExportPreferencesHandler {

	@CanExecute
	public boolean canExecute(@Optional PreferencesAuthorization prefAuthService) {
		boolean isAllowed = true;
		if (prefAuthService != null) {
			isAllowed = prefAuthService.exportAllowed();
		}
		return isAllowed;
	}

	@Execute
	public void execute(@Optional PreferencesAuthorization prefAuthService,
			IEclipseContext context) {
		if (!canExecute(prefAuthService))
			return;

		ContextInjectionFactory.make(ExportPreferences.class, context);
	}

}
