/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.vaaclipse.ui.preferences.addon.internal.service;

import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.eclipse.e4.core.di.annotations.Optional;
import org.osgi.service.prefs.Preferences;
import org.osgi.service.prefs.PreferencesService;
import org.eclipse.osbp.vaaclipse.publicapi.preferences.IPreferenceProvider;

public class PreferenceProvider implements IPreferenceProvider {

	@Inject
	@Named("userId")
	@Optional
	String userId;

	@Inject
	private PreferencesService delegate;

	@PostConstruct
	public void init() {
		if (userId == null) {
			userId = UUID.randomUUID().toString();
		}
	}

	@Override
	public Preferences getSystemPreferences() {
		return delegate.getSystemPreferences();
	}

	@Override
	public Preferences getUserPreferences() {
		return getUserPreferences(userId);
	}

	@Override
	public Preferences getUserPreferences(String userId) {
		return delegate.getUserPreferences(userId);
	}

}
