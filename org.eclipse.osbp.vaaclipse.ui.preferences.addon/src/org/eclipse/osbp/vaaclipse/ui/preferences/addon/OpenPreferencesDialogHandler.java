/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.vaaclipse.ui.preferences.addon;

import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.osbp.vaaclipse.ui.preferences.addon.internal.PreferencesDialog;

import com.vaadin.ui.UI;

import e4modelextension.VaaclipseApplication;

/**
 * @author rushan
 *
 */
public class OpenPreferencesDialogHandler {

	@Execute
	public void execute(IEclipseContext context, MApplication app, UI ui) {

		context.set(VaaclipseApplication.class, (VaaclipseApplication) app);
		PreferencesDialog prefDlg = ContextInjectionFactory.make(
				PreferencesDialog.class, context);
		prefDlg.getWindow().center();
		ui.addWindow(prefDlg.getWindow());
	}

}
