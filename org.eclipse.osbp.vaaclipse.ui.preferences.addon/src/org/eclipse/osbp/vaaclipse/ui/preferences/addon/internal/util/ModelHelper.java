/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.vaaclipse.ui.preferences.addon.internal.util;

import java.util.List;

import org.eclipse.osbp.vaaclipse.ui.preferences.model.PreferencesCategory;

/**
 * @author rushan
 *
 */
public class ModelHelper {

	public static void buildChildCategoryListIncludeThis(
			PreferencesCategory category, List<PreferencesCategory> childList) {
		childList.add(category);
		for (PreferencesCategory child : category.getChildCategories()) {
			buildChildCategoryListIncludeThis(child, childList);
		}
	}

	public static void buildChildCategoryListIncludeThisList(
			List<PreferencesCategory> list, List<PreferencesCategory> childList) {
		for (PreferencesCategory c : list) {
			buildChildCategoryListIncludeThis(c, childList);
		}
	}

}
