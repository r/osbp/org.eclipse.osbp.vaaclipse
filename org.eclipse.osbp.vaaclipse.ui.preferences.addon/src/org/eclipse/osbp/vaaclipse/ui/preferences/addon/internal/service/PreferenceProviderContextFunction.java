/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.vaaclipse.ui.preferences.addon.internal.service;

import org.eclipse.e4.core.contexts.ContextFunction;
import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IContextFunction;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.osgi.service.component.annotations.Component;
import org.eclipse.osbp.vaaclipse.publicapi.preferences.IPreferenceProvider;

/**
 * Use this class to obtain an instance of {@link IPreferenceProvider}.
 */
@Component(service = IContextFunction.class, property = { "service.context.key=org.eclipse.osbp.vaaclipse.publicapi.preferences.IPreferenceProvider" })
public class PreferenceProviderContextFunction extends ContextFunction {
	@Override
	public Object compute(IEclipseContext context, String contextKey) {
		PreferenceProvider broker = ContextInjectionFactory.make(
				PreferenceProvider.class, context);
		context.set(IPreferenceProvider.class, broker);
		context.set(PreferenceProvider.class, broker);
		return broker;
	}
}
