/*******************************************************************************
 * Copyright (c) 2012 Rushan R. Gilmullin and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Rushan R. Gilmullin - initial API and implementation
 *******************************************************************************/
 package org.eclipse.osbp.vaaclipse.api;

import org.eclipse.e4.ui.model.application.ui.menu.MMenu;

/**
 * @author rushan
 *
 */
public interface MenuContributionService {
	void addContributions(MMenu menu);

	void removeContributions(MMenu menu);
}
