/*******************************************************************************
 * Copyright (c) 2012 Rushan R. Gilmullin and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Rushan R. Gilmullin - initial API and implementation
 *******************************************************************************/

package org.eclipse.osbp.vaaclipse.api;

/**
 * @author rushan
 *
 */
public class Events {
	public static class MinMaxEvents {
		public static final String EVENT_MAXIMIZE_ELEMENT = "EVENT_MAXIMIZE_ELEMENT";
		public static final String EVENT_MINIMIZE_ELEMENT = "EVENT_MINIMIZE_ELEMENT";
		public static final String EVENT_RESTORE_ELEMENT = "EVENT_RESTORE_ELEMENT";

		public static final String PARAMETER_ELEMENT = "PARAMETER_ELEMENT";
		public static final String PARAMETER_TRIMBAR = "PARAMETER_TRIMBAR";
		public static final String PARAMETER_TOOLBAR = "PARAMETER_TOOLBAR";
	}

}
