/*******************************************************************************
 * Copyright (c) 2012 Rushan R. Gilmullin and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Rushan R. Gilmullin - initial API and implementation
 *******************************************************************************/
 package org.eclipse.osbp.vaaclipse.theme;

import org.eclipse.osbp.vaaclipse.publicapi.theme.ThemeContribution;

/**
 * @author rushan
 *
 */
public class ThemeContributionImpl extends ThemeEntryImpl implements
		ThemeContribution {
	private String insertPosition = "after=MAIN_CSS";

	public ThemeContributionImpl(String id) {
		super(id);
	}

	@Override
	public String getInsertPosition() {
		return insertPosition;
	}

	public void setInsertPosition(String insertPosition) {
		this.insertPosition = insertPosition;
	}
}
