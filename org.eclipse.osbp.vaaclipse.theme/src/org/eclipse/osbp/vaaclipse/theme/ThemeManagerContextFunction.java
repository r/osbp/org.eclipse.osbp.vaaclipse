/*******************************************************************************
 * Copyright (c) 2012 Rushan R. Gilmullin and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Rushan R. Gilmullin - initial API and implementation
 *******************************************************************************/
 package org.eclipse.osbp.vaaclipse.theme;

import org.eclipse.e4.core.contexts.ContextFunction;
import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.osbp.vaaclipse.publicapi.theme.ThemeEngine;
import org.eclipse.osbp.vaaclipse.publicapi.theme.ThemeManager;

/**
 * @author rushan
 *
 */
public class ThemeManagerContextFunction extends ContextFunction {
	private ThemeEngine themeEngine;

	public void activate() {
		System.out.println("Theme manager factory is started");
	}

	@Override
	public Object compute(IEclipseContext context) {
		ThemeManager manager = context.getLocal(ThemeManager.class);
		if (manager == null) {
			if (themeEngine != null)
				context.set(ThemeEngine.class, themeEngine);
			manager = ContextInjectionFactory.make(ThemeManagerImpl.class,
					context);
			context.set(ThemeManager.class, manager);
		}
		return manager;
	}

	public void bindThemeEngine(ThemeEngine themeEngine) {
		this.themeEngine = themeEngine;
	}
}
