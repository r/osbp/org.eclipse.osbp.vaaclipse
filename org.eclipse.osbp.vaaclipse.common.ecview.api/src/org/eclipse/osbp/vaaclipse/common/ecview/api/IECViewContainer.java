/*******************************************************************************
 * Copyright (c) 2012 Rushan R. Gilmullin and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Rushan R. Gilmullin - initial API and implementation
 *******************************************************************************/
package org.eclipse.osbp.vaaclipse.common.ecview.api;

import java.util.List;

import org.eclipse.e4.ui.model.application.ui.advanced.MPerspective;
import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.core.common.editpart.IViewEditpart;

/**
 * Keeps track of active ecview instances.
 */
public interface IECViewContainer {

	/**
	 * Returns a list with all ECViews registered with this Vaaclipse session.
	 * 
	 * @return
	 */
	List<IViewEditpart> getECViews();

	List<IViewContext> getECViews(MPerspective perspective);

}
