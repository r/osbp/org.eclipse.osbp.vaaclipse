/*******************************************************************************
 * Copyright (c) 2012 Rushan R. Gilmullin and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Rushan R. Gilmullin - initial API and implementation
 *******************************************************************************/
 package org.eclipse.osbp.vaaclipse.common.ecview.api;

import java.util.Map;

/**
 * Instances of this service need to be created in a Vaaclipse UI scoped manner.
 * Every vaadin.UI will get its own instance.<br>
 * So session and UI based information like locale, userName,... are available.
 * 
 * @NoImplement by clients
 */
public interface IECViewSessionHelper {

	/**
	 * Creates the basic properties required to setup a proper IViewContext in
	 * ECView.
	 * 
	 * @return
	 */
	public abstract Map<String, Object> createBasicProperties();

}
