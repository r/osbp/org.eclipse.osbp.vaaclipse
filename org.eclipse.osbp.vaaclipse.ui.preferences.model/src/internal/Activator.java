/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package internal;

import org.eclipse.osbp.vaaclipse.ui.preferences.model.impl.PreferencesFactoryImpl;
import org.eclipse.osbp.vaaclipse.ui.preferences.model.metadata.PreferencesFactory;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

/**
 * @author rushan
 *
 */
public class Activator implements BundleActivator {

	private ServiceRegistration<PreferencesFactory> factoryRegistraction;

	@Override
	public void start(BundleContext context) throws Exception {
		PreferencesFactory factory = new PreferencesFactoryImpl();
		factoryRegistraction = context.registerService(
				PreferencesFactory.class, factory, null);
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		factoryRegistraction.unregister();
	}
}
