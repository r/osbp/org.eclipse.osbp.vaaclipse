/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.vaaclipse.ui.preferences.model;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>String Field Editor</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 * <li>
 * {@link org.eclipse.osbp.vaaclipse.ui.preferences.model.StringFieldEditor#getMaxLength
 * <em>Max Length</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.osbp.vaaclipse.ui.preferences.model.metadata.PreferencesPackage#getStringFieldEditor()
 * @model superTypes=
 *        "org.eclipse.osbp.vaaclipse.ui.preferences.model.FieldEditor<org.eclipse.emf.ecore.EString>"
 * @generated
 */
public interface StringFieldEditor extends FieldEditor<String> {
	/**
	 * Returns the value of the '<em><b>Max Length</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Length</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Max Length</em>' attribute.
	 * @see #setMaxLength(Integer)
	 * @see org.eclipse.osbp.vaaclipse.ui.preferences.model.metadata.PreferencesPackage#getStringFieldEditor_MaxLength()
	 * @model
	 * @generated
	 */
	Integer getMaxLength();

	/**
	 * Sets the value of the '
	 * {@link org.eclipse.osbp.vaaclipse.ui.preferences.model.StringFieldEditor#getMaxLength
	 * <em>Max Length</em>}' attribute. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Max Length</em>' attribute.
	 * @see #getMaxLength()
	 * @generated
	 */
	void setMaxLength(Integer value);

} // StringFieldEditor
