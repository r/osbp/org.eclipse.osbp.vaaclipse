/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.vaaclipse.ui.preferences.model;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Boolean Field Editor</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 * <li>
 * {@link org.eclipse.osbp.vaaclipse.ui.preferences.model.BooleanFieldEditor#getStyle
 * <em>Style</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.eclipse.osbp.vaaclipse.ui.preferences.model.metadata.PreferencesPackage#getBooleanFieldEditor()
 * @model superTypes=
 *        "org.eclipse.osbp.vaaclipse.ui.preferences.model.FieldEditor<org.eclipse.emf.ecore.EBooleanObject>"
 * @generated
 */
public interface BooleanFieldEditor extends FieldEditor<Boolean> {
	/**
	 * Returns the value of the '<em><b>Style</b></em>' attribute. The literals
	 * are from the enumeration
	 * {@link org.eclipse.osbp.vaaclipse.ui.preferences.model.BooleanFieldStyle}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Style</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Style</em>' attribute.
	 * @see org.eclipse.osbp.vaaclipse.ui.preferences.model.BooleanFieldStyle
	 * @see #setStyle(BooleanFieldStyle)
	 * @see org.eclipse.osbp.vaaclipse.ui.preferences.model.metadata.PreferencesPackage#getBooleanFieldEditor_Style()
	 * @model
	 * @generated
	 */
	BooleanFieldStyle getStyle();

	/**
	 * Sets the value of the '
	 * {@link org.eclipse.osbp.vaaclipse.ui.preferences.model.BooleanFieldEditor#getStyle
	 * <em>Style</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Style</em>' attribute.
	 * @see org.eclipse.osbp.vaaclipse.ui.preferences.model.BooleanFieldStyle
	 * @see #getStyle()
	 * @generated
	 */
	void setStyle(BooleanFieldStyle value);

} // BooleanFieldEditor
