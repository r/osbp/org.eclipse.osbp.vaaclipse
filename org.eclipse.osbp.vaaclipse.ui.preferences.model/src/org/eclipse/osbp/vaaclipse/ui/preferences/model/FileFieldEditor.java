/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.vaaclipse.ui.preferences.model;

import org.eclipse.e4.ui.model.application.MContribution;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>File Field Editor</b></em>'. <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.osbp.vaaclipse.ui.preferences.model.metadata.PreferencesPackage#getFileFieldEditor()
 * @model superTypes=
 *        "org.eclipse.osbp.vaaclipse.ui.preferences.model.FieldEditor<org.eclipse.emf.ecore.EString> org.eclipse.e4.ui.model.application.Contribution"
 * @generated
 */
public interface FileFieldEditor extends FieldEditor<String>, MContribution {
} // FileFieldEditor
