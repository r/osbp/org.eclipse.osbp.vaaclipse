/*******************************************************************************
 * Copyright (c) 2012 Rushan R. Gilmullin and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Rushan R. Gilmullin - initial API and implementation
 *******************************************************************************/

package org.eclipse.osbp.vaaclipse.emf.handler;

import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.osbp.vaaclipse.emf.parts.EmfModelTreePart;

/**
 * This handler shows the previous EObject in the tree viewer. Will show
 * eObject.eContainer().
 */
public class EmfTreeViewPreviousElementHandler {

	@Execute
	public void execute(MPart part) {
		Object object = part.getObject();
		if (object != null) {
			IEclipseContext childContext = part.getContext().createChild();
			childContext.set(EmfModelTreePart.EVENT_TYPE_NAME,
					EmfModelTreePart.EventType.PREVIOUS);
			ContextInjectionFactory.invoke(object, Execute.class, childContext);
		}
	}
}
