/*******************************************************************************
 * Copyright (c) 2012 Rushan R. Gilmullin and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Rushan R. Gilmullin - initial API and implementation
 *******************************************************************************/

 package org.eclipse.osbp.vaaclipse.emf;

import org.eclipse.e4.core.contexts.ContextFunction;
import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.osbp.vaadin.emf.api.IModelingContext;
import org.osgi.service.component.annotations.Component;

@Component(service = org.eclipse.e4.core.contexts.IContextFunction.class, property = { "service.context.key=org.eclipse.osbp.vaadin.emf.api.IModelingContext" })
public class ModelingContextContextFunction extends ContextFunction {

	@Override
	public Object compute(IEclipseContext context, String contextKey) {
		// add the new object to the application context
		MApplication application = context.get(MApplication.class);
		IEclipseContext appCtx = application.getContext();

		IModelingContext modelingContext = ContextInjectionFactory.make(
				ModelingContext.class, appCtx);
		appCtx.set(IModelingContext.class, modelingContext);
		return modelingContext;
	}
}
