/*******************************************************************************
 * Copyright (c) 2012 Rushan R. Gilmullin and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Rushan R. Gilmullin - initial API and implementation
 *******************************************************************************/
 package org.eclipse.osbp.vaaclipse.emf;

import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;

import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.emf.common.command.BasicCommandStack;
import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.osbp.vaadin.emf.api.IModelingContext;
import org.eclipse.osbp.vaaclipse.publicapi.events.IWidgetModelAssociations;

public class ModelingContext implements IModelingContext {

	@Inject
	private IWidgetModelAssociations uiAssociation;

	@Inject
	private MApplication application;

	private AdapterFactoryEditingDomain domain;

	private boolean designMode;

	private CustomCommandStack commandStack;

	@PostConstruct
	protected void setup() {
		commandStack = new CustomCommandStack();
		domain = new AdapterFactoryEditingDomain(new ComposedAdapterFactory(
				ComposedAdapterFactory.Descriptor.Registry.INSTANCE),
				commandStack);

		// register the domain in the context
		application.getContext().set(EditingDomain.class, domain);
	}

	@Override
	public void loadPerspective(URI uri) {
		domain.loadResource(uri.toString());
	}

	@Override
	public void setPerspective(URI uri) {
		domain.loadResource(uri.toString());
	}

	@Override
	public EditingDomain getEditingDomain() {
		return domain;
	}

	@PreDestroy
	protected void destroy() {
		// register the domain in the context
		application.getContext().remove(EditingDomain.class);
		domain = null;
	}

	@Override
	public AdapterFactory getAdapterFactory() {
		return domain.getAdapterFactory();
	}

	@Override
	public CommandStack getCommandStack() {
		return commandStack;
	}

	public void persist(URI uri, EObject eObject) {

	}

	public EObject getModelForUi(Object component) {
		return uiAssociation.getElement(component);
	}

	@Override
	public boolean isDesignMode() {
		return designMode;
	}

	@Override
	public void setDesignMode(boolean value) {
		this.designMode = value;
	}

	public static class CustomCommandStack extends BasicCommandStack implements
			IModelingContext.CommandStack {
		@Override
		public List<Command> getAllCommands() {
			return Collections.unmodifiableList(commandList);
		}

	}

}
