/*******************************************************************************
 * Copyright (c) 2012 Rushan R. Gilmullin and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Rushan R. Gilmullin - initial API and implementation
 *******************************************************************************/
 package org.eclipse.osbp.vaaclipse.emf.dialog;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.osbp.vaaclipse.emf.api.dialog.IOpenModelEditDialogService;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;

@Component
public class OpenModelEditDialog implements IOpenModelEditDialogService {

	private Set<Participant> participants = Collections
			.synchronizedSet(new HashSet<Participant>());

	@Override
	public void openEditDialog(EObject modelObject, String type,
			IEclipseContext context) {
		synchronized (participants) {
			for (Participant participant : participants) {
				if (participant.isFor(modelObject, type, context)) {
					participant.openEditDialog(modelObject, type, context);
				}
			}
		}
	}

	/**
	 * Called by OSGi-DS
	 * 
	 * @param participant
	 */
	@Reference(cardinality = ReferenceCardinality.MULTIPLE, policy = ReferencePolicy.DYNAMIC)
	protected void addParticipant(Participant participant) {
		participants.add(participant);
	}

	protected void removeParticipant(Participant participant) {
		participants.remove(participant);
	}

}
