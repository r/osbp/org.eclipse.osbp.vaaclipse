/*******************************************************************************
 * Copyright (c) 2012 Rushan R. Gilmullin and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Rushan R. Gilmullin - initial API and implementation
 *******************************************************************************/
 package org.eclipse.osbp.vaaclipse.emf.parts;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;
import javax.inject.Named;

import org.eclipse.e4.core.contexts.Active;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.model.application.ui.advanced.MPerspective;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.osbp.runtime.designer.api.IWidgetDesignConfigurator;
import org.eclipse.osbp.runtime.web.vaadin.common.resource.IResourceProvider;
import org.eclipse.osbp.vaaclipse.emf.api.view.IConstants;
import org.eclipse.osbp.vaaclipse.publicapi.events.IWidgetModelAssociations;
import org.eclipse.osbp.vaadin.emf.actions.EObjectActionHandler;
import org.eclipse.osbp.vaadin.emf.actions.EObjectActionHandler.EditCallback;
import org.eclipse.osbp.vaadin.emf.api.EmfModelElementClickEvent;
import org.eclipse.osbp.vaadin.emf.api.IModelingConstants;
import org.eclipse.osbp.vaadin.emf.api.IModelingContext;
import org.eclipse.osbp.vaadin.emf.views.EmfModelTreeViewer;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventHandler;

import com.vaadin.ui.VerticalLayout;

/**
 * A tree viewer which can show any EMF Model.
 */
public class EmfModelTreePart {

	public final static String EVENT_TYPE_NAME = "eventType";

	@Inject
	private IModelingContext modelingContext;

	@Inject
	private IEventBroker eventBroker;

	@Inject
	private IEclipseContext context;

	@Inject
	private IResourceProvider resourceProvider;

	@Inject
	private VerticalLayout parent;

	// @Inject
	// private IOpenModelEditDialogService dialogOpener;

	@Inject
	private IWidgetDesignConfigurator dialogOpener;

	@Inject
	private IWidgetModelAssociations associations;

	private EmfModelTreeViewer viewer;

	private EventHandler selectedEObject;

	/**
	 * Locks selections
	 */
	private boolean locked = false;

	private EditCallback editCallback;

	public EmfModelTreePart() {

	}

	@PostConstruct
	protected void setup() {
		// opens a dialog to edit a model element
		editCallback = new EObjectActionHandler.EditCallback() {
			@Override
			public void edit(EObject model) {
				// dialogOpener.openEditDialog(model,
				// context);
				dialogOpener.openEditDialog(associations.getWidget(model),
						model);
			}
		};
		viewer = new EmfModelTreeViewer(modelingContext, resourceProvider,
				editCallback);
		viewer.setSizeFull();
		parent.addComponent(viewer);

		viewer.addItemClickListener(e -> {
			eventBroker.send(IConstants.TOPIC__TREE_SELECTION_OUT,
					e.getItemId());
		});

		selectedEObject = new EventHandler() {
			@Override
			public void handleEvent(Event event) {
				if (locked) {
					return;
				}

				Object data = event.getProperty(IEventBroker.DATA);
				if (data instanceof EmfModelElementClickEvent) {
					EmfModelElementClickEvent clickData = (EmfModelElementClickEvent) data;
					viewer.setRootElement((EObject) clickData.getModelElement());
					viewer.expand(viewer.getRootElement());
					viewer.setSelection(viewer.getRootElement());
				} else {
					EObject eObject = (EObject) event
							.getProperty(IEventBroker.DATA);
					viewer.setRootElement(eObject);
					viewer.expand(viewer.getRootElement());
					viewer.setSelection(viewer.getRootElement());
				}
			}
		};

		eventBroker.subscribe(IModelingConstants.TOPIC__ELEMENT_CLICKED,
				selectedEObject);

	}

	@Inject
	protected void activePerspective(@Optional @Active MPerspective mPerspective) {
		// if (viewer != null) {
		// EObject root = EcoreUtil
		// .getRootContainer((EObject) mPerspective);
		// viewer.setRootElement(root);
		// viewer.expand(root);
		// }
	}

	@Execute
	protected void executeEvent(@Named(EVENT_TYPE_NAME) EventType type) {
		if (type == EventType.PREVIOUS) {
			EObject root = viewer.getRootElement();
			if (root != null) {
				EObject newRoot = root.eContainer();
				if (newRoot != null) {
					EObject selection = viewer.getSelection();
					viewer.setRootElement(newRoot);
					if (selection != null) {
						viewer.expand(selection);
						viewer.setSelection(selection);
					}
				}
			}
		} else if (type == EventType.LOCK) {
			locked = !locked;
		}
	}

	@PreDestroy
	protected void destroy() {
		eventBroker.unsubscribe(selectedEObject);
		selectedEObject = null;

		// clear all caches in the viewer
		viewer.setRootElement(null);
		viewer.dispose();
		viewer = null;
	}

	public enum EventType {
		/**
		 * show previous EObject
		 */
		PREVIOUS,
		/**
		 * Locks the selection
		 */
		LOCK
	}
}
