/*******************************************************************************
 * Copyright (c) 2012 Rushan R. Gilmullin and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Rushan R. Gilmullin - initial API and implementation
 *******************************************************************************/
 package org.eclipse.osbp.vaaclipse.emf.parts;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;

import org.eclipse.e4.core.contexts.Active;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.model.application.ui.advanced.MPerspective;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.osbp.runtime.web.vaadin.common.resource.IResourceProvider;
import org.eclipse.osbp.vaaclipse.emf.api.view.IConstants;
import org.eclipse.osbp.vaadin.emf.api.IModelingContext;
import org.eclipse.osbp.vaadin.emf.views.EmfPropertyViewer;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventHandler;

import com.vaadin.ui.VerticalLayout;

/**
 * A properties view which can show the properties of any EObject.
 */
public class EmfPropertiesPart {

	@Inject
	private IModelingContext modelingContext;

	@Inject
	private IEventBroker eventBroker;

	@Inject
	private IResourceProvider resourceProvider;

	@Inject
	private VerticalLayout parent;

	private EmfPropertyViewer viewer;

	private EventHandler selectedEObject;

	public EmfPropertiesPart() {

	}

	@PostConstruct
	protected void setup() {
		viewer = new EmfPropertyViewer(modelingContext, resourceProvider);
		viewer.setSizeFull();
		parent.addComponent(viewer);

		selectedEObject = new EventHandler() {
			@Override
			public void handleEvent(Event event) {
				EObject eObject = (EObject) event
						.getProperty(IEventBroker.DATA);
				viewer.setRootElement(eObject);
			}
		};

		eventBroker.subscribe(IConstants.TOPIC__TREE_SELECTION_OUT,
				selectedEObject);

	}

	@Inject
	protected void activePerspective(@Optional @Active MPerspective mPerspective) {
		if (viewer != null) {
			viewer.setRootElement(EcoreUtil
					.getRootContainer((EObject) mPerspective));
		}
	}

	@PreDestroy
	protected void destroy() {
		eventBroker.unsubscribe(selectedEObject);
		selectedEObject = null;

		// clear all caches in the viewer
		viewer.setRootElement(null);
		viewer = null;
	}
}
