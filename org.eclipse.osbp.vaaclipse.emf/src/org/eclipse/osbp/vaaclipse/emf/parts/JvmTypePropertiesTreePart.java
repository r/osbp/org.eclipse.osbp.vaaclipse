/*******************************************************************************
 * Copyright (c) 2012 Rushan R. Gilmullin and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Rushan R. Gilmullin - initial API and implementation
 *******************************************************************************/
 package org.eclipse.osbp.vaaclipse.emf.parts;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;

import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.xtext.common.types.JvmGenericType;
import org.eclipse.osbp.dsl.xtext.types.bundles.BundleSpaceTypeProvider;
import org.eclipse.osbp.runtime.common.types.IBundleSpace;
import org.eclipse.osbp.runtime.web.vaadin.common.resource.IResourceProvider;
import org.eclipse.osbp.vaadin.emf.api.IModelingContext;
import org.eclipse.osbp.vaadin.emf.views.JvmTypePropertiesTreeViewer;
import org.osgi.service.event.EventHandler;

import com.vaadin.ui.VerticalLayout;

public class JvmTypePropertiesTreePart {

	public final static String EVENT_TYPE_NAME = "eventType";

	@Inject
	private IModelingContext modelingContext;

	@Inject
	private IEclipseContext context;

	@Inject
	private IEventBroker eventBroker;

	@Inject
	private IResourceProvider resourceProvider;

	@Inject
	private VerticalLayout parent;

	private JvmTypePropertiesTreeViewer viewer;

	private EventHandler selectedEObject;

	public JvmTypePropertiesTreePart() {

	}

	@SuppressWarnings("restriction")
	@PostConstruct
	protected void setup() {

		BundleSpaceTypeProvider typeProvider = new BundleSpaceTypeProvider(
				context.get(IBundleSpace.class), modelingContext
						.getEditingDomain().getResourceSet(), null);

		viewer = new JvmTypePropertiesTreeViewer(modelingContext,
				resourceProvider, typeProvider);
		viewer.setSizeFull();
		parent.addComponent(viewer);

		viewer.setRootElement("com.osbp.myfirstapp.dtos.CompanyDto");

		// selectedEObject = new EventHandler() {
		// @Override
		// public void handleEvent(Event event) {
		// Object data = event.getProperty(IEventBroker.DATA);
		// if (data instanceof EmfModelElementClickEvent) {
		// EmfModelElementClickEvent clickData = (EmfModelElementClickEvent)
		// data;
		// viewer.setRootElement((EObject) clickData.getModelElement());
		// viewer.expand(viewer.getRootElement());
		// viewer.setSelection(viewer.getRootElement());
		// } else {
		// EObject eObject = (EObject) event
		// .getProperty(IEventBroker.DATA);
		// viewer.setRootElement(eObject);
		// viewer.expand(viewer.getRootElement());
		// viewer.setSelection(viewer.getRootElement());
		// }
		// }
		// };
	}

	@PreDestroy
	protected void destroy() {
		// eventBroker.unsubscribe(selectedEObject);
		selectedEObject = null;

		// clear all caches in the viewer
		viewer.setRootElement((JvmGenericType) null);
		viewer.dispose();
		viewer = null;
	}
}
