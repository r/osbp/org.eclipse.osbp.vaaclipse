/*******************************************************************************
 * Copyright (c) 2012 Rushan R. Gilmullin and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Rushan R. Gilmullin - initial API and implementation
 *******************************************************************************/
 package org.eclipse.osbp.vaaclipse.emf.parts;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;

import org.eclipse.osbp.vaadin.emf.api.IModelingContext;
import org.eclipse.osbp.vaadin.emf.views.EmfCommandStackTableViewer;

import com.vaadin.ui.VerticalLayout;

public class EmfCommandStackPart {

	@Inject
	private IModelingContext modelingContext;

	@Inject
	private VerticalLayout parent;

	private EmfCommandStackTableViewer viewer;

	public EmfCommandStackPart() {

	}

	@PostConstruct
	protected void setup() {
		viewer = new EmfCommandStackTableViewer(modelingContext);
		viewer.setSizeFull();
		parent.addComponent(viewer);
	}

	// @Execute
	// protected void execute(@Named(EVENT_TYPE_NAME) EventType type) {
	// ECViewBindingDialog.open(viewer.getRootElement(), context, null, this);
	// }

	@PreDestroy
	protected void destroy() {
		// clear all caches in the viewer
		viewer.dispose();
		viewer = null;
	}

	public static enum EventType {
		UNDO, REDO
	}

}
