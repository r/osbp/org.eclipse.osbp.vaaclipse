/*******************************************************************************
 * Copyright (c) 2012 Rushan R. Gilmullin and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Rushan R. Gilmullin - initial API and implementation
 *******************************************************************************/
 package org.eclipse.osbp.vaaclipse.addon;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.osbp.vaaclipse.common.ecview.api.IECViewContainer;
import org.eclipse.osbp.vaaclipse.emf.ModeledUiClickController;
import org.eclipse.osbp.vaadin.emf.api.IModelingContext;

/**
 * Activates the whole EMF modeling addon and ensures, that require parts are
 * injected and setup properly.
 */
@SuppressWarnings("restriction")
public class EMFAddon {

	@Inject
	private IEclipseContext context;

	@PostConstruct
	protected void setup() {

		context.get(IModelingContext.class);
		context.get(IECViewContainer.class);

		ModeledUiClickController controller = ContextInjectionFactory.make(
				ModeledUiClickController.class, context);
		context.set(ModeledUiClickController.class, controller);
	}

}
